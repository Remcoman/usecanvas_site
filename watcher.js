const config = require('./config');
const path = require('path');
const co = require('co');
const coFilter = require('co-filter');
const fs = require('co-fs');
const Remarkable = require('remarkable');
const ejs = require('ejs');
const utils = require('./libs/utils');
const cache = require('./libs/cache')('./page-cache');

const mdRenderer = new Remarkable({
    html : true
}); 
 
let cachedMetadata, 
    desktopPageTemplate,
    desktopSummaryTemplate;

/**
 * We need to make sure that the .cache file always reflects the most recent config.urls
 */
const loadCachedMetadata = co.wrap(function* () {
    const metadata = yield cache.read();

    //build an hash map for all urls which are in metadata.items
    const metadataURLs = {};
    metadata.items.forEach(item => metadataURLs[item.url] = true);

    //get only the missing urls
    const missingURLs = config.urls.filter(url => !(url in metadataURLs));

    if(missingURLs.length) {
        const missingEntries = yield missingURLs.map(url => {
            return utils.requestPromise(url + ".json")
                .then(({body}) => {
                    const jsonData = JSON.parse(body);
                    return {url, slug : jsonData.title_slug};
                });
        });
        metadata.add(missingEntries);
        yield metadata.write();
    }

    return metadata;
});

/**
 * We do an HEAD request to each url to get the most recent etag.
 * We use this etag to determine if the contents of the page has changed
 * This function will return the changed items
 */
const getChangedPages = () => {
    const changedPageRequests = cachedMetadata.items.map(metadata => {
        return utils.requestPromise(metadata.url + ".json", {method : "HEAD"})
            .then(({response}) => {
                if(process.env.NODE_ENV === "development" || 
                   response.headers["etag"] !== metadata.etag) {
                    return Object.assign({}, metadata, {etag : response.headers["etag"]});
                }
                else {
                    return null;
                }
            });
    });

    return Promise.all(changedPageRequests)
        .then(changedPages => changedPages.filter(item => item !== null));
};

/**
 * This function requests the data for the json and markdown urls
 */
const getDataForPage = (page) => {
    return Promise.all([
            utils.requestPromise(page.url + ".json"), 
            utils.requestPromise(page.url + ".md")
        ])
        .then(([jsonResponse, mdResponse]) => {
            const {
                title, 
                summary, 
                creator : {username : creator_name, avatar_url : creator_thumb}
            } = JSON.parse(jsonResponse.body);

            const metadata = Object.assign({}, page, {
                title,
                summary,
                creator_name,
                creator_thumb
            });

            return {metadata, md : mdResponse.body};
        });
};

/**
 * This function writes the page content to the page-cache
 */
const writePageCache = co.wrap(function* (destPath, {metadata, md}) {
    const html = yield desktopPageTemplate( {metadata, pageContents : mdRenderer.render( md )} );
    return fs.writeFile(destPath, html);
});

/**
 * This function writes the summary cache
 */
const writeSummaryCache = metadataItems => {
    return desktopSummaryTemplate( {metadataItems} )
        .then(html => fs.writeFile("page-cache/summary", html));
}

const updateCache = co.wrap(function* (pages) {
    let changedPages;
    try {
        changedPages = yield pages.map(page => getDataForPage(page));
    }
    catch(e) {
        console.error("Could not get updated page data", e);
        return;
    }

    //write the page templates for all the changed pages
    try {
        yield changedPages.map(item => {
            const destPath = path.join("page-cache", item.metadata.slug);
            return writePageCache(destPath, item);
        });
    }
    catch(e) {
        console.error("Could not write updated page data", e);
        return;
    }

    const changedPageMetadata = changedPages.map(item => item.metadata);
    cachedMetadata.add(changedPageMetadata);

    //we need to write the summary for all items (not for only the changed items)
    try {
        yield writeSummaryCache(cachedMetadata.items);
    }
    catch(e) {
        console.error("Could not write summary!", e);
        return;
    }

    yield cachedMetadata.write();
});

const poll = co.wrap(function*() {
    let changes;
    try {
        changes = yield getChangedPages(); 
    }
    catch(e) {
        console.error("Could not get changed pages!", e);
    }

    if(changes) {
        if(changes.length) {
            yield updateCache(changes);
        }
        else {
            console.info("Nothing changed");
        }
    }

    setTimeout(poll, config.pollInterval);
});

const templateRenderer = function (filePath) {
    let precompiled = null;

    return co.wrap(function* (data) {
        if(!precompiled || process.env.NODE_ENV === "development") {
            precompiled = ejs.compile( yield fs.readFile(filePath, "utf8"), {filename : filePath} );
        }
        return precompiled(data);
    });
};

co(function*() {
    cachedMetadata = yield loadCachedMetadata();
    desktopPageTemplate = templateRenderer('./templates/desktop-page.ejs');
    desktopSummaryTemplate = templateRenderer('./templates/desktop-summary.ejs');

    //start polling
    poll();
});