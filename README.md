# Simple static site generator

This static site generator uses data from usecanvas to generate static versions of each page. 
Including an summary. It works by launching two services: the server and the watcher.

## The watcher

The watcher polls the urls in the config for new data by comparing the old etag vs the new one.
If it has changed then we collect both the json and markdown versions of each page and 
regenerate the new page view + summary. We will also the save metadata into the .cache file.

## The server

The server launches an http server on port 8080 and allows you to visit each page by its slug.

## How to use

To launch in development mode (requires docker + docker-compose):

    npm install
    npm start

## Technologies used

- Node.js
- Koa (for the http server)
- Sass (for the styling)
- Ejs (for the templates)
- Docker (for launching the services)
- Docker-compose (for managing the watcher and server services)