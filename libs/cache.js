const co = require('co');
const fs = require('co-fs');

const CacheProto = {
    add(items) {
        items.forEach(item => {
            let itemCopy = Object.assign({}, item);
            delete itemCopy.slug;
            this[item.slug] = itemCopy;
        });
    },

    write() {
        return fs.writeFile(this.path + "/.cache", JSON.stringify(this, undefined, "\t"));
    },
    
    get items() {
        return Object.keys(this).map(slug => {
            return Object.assign({slug}, this[slug]);
        });
    }
}

module.exports = (path) => {
    return {
        create() {
            return Object.create(CacheProto, {
                path : {
                    value : path
                }
            });
        },

        read : co.wrap(function *() {
            const cache = this.create();
            if(yield fs.exists(path + "/.cache")) {
                const contents = yield fs.readFile(path + "/.cache", "utf8");
                Object.assign(cache, JSON.parse(contents));
            }
            return cache;
        })
    }
}