const request = require('request');

exports.requestPromise = function(...args) {
    return new Promise((resolve, reject) => {
        request(...args, (error, response, body) => {
            error ? reject(error) : resolve({response, body});
        });
    });
}