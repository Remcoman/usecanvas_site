const koa = require('koa');
const koaRouter = require('koa-router');
const koaStatic = require('koa-static');
const request = require('request');
const co = require('co');
const coFs = require('co-fs');
const fs = require('fs');
const path = require('path');

const app = koa();
const router = koaRouter();

router.get(`/`, function* () {
    this.status = 200;
    this.type = "text/html";
    this.body = fs.createReadStream("./page-cache/summary", {encoding : "utf8"} );
});

router.get('/:slug', function* () {
    let filePath = path.join("./page-cache", this.params.slug);
    const exists = yield coFs.exists(filePath);
    if(!exists) {
        this.status = 404;
        return;
    }

    this.status = 200;
    this.type = "text/html";
    this.body = fs.createReadStream(filePath, {encoding : "utf8"} );
});

app.use(koaStatic("./public"));
app.use(router.routes());

console.log(`Starting server on port ${process.env.SERVER_PORT}`);

app.listen(process.env.SERVER_PORT);