FROM node:alpine

RUN npm install pm2 -g

ENV HOME=/usr/src/app

WORKDIR $HOME

RUN addgroup -S app && adduser -S -g app app

#npm install stuff
COPY package.json npm-shrinkwrap.json $HOME/
COPY style $HOME/style
COPY public $HOME/public 
COPY watcher.js server.js config.js $HOME/
COPY libs $HOME/libs
COPY templates $HOME/templates

RUN chown -R app:app $HOME
USER app
RUN npm install
RUN mkdir $HOME/page-cache
RUN npm run-script build-css

EXPOSE 8080